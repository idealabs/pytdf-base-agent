#!/usr/bin/env python

from setuptools import setup
from setuptools.command.test import test as TestCommand
import sys


class PyTest(TestCommand):

    def initialize_options(self):
        TestCommand.initialize_options(self)

    def finalize_options(self):
        TestCommand.finalize_options(self)

        self.test_args = ['app', '--cov', 'app', '--cov-report',
                          'term-missing']
        self.test_suite = True

    def run_tests(self):
        import pytest
        errno = pytest.main(self.test_args)
        sys.exit(errno)

setup(
    name='pytdf-base-agent',
    version='0.0.1',
    packages=['base_agent'],
    include_package_data=True,
    install_requires=[
        'numpy',
        'pandas',
        'requests'
    ],
    tests_require=[
        'pytest',
        'pytest-cov'
    ],
    cmdclass={
        'test': PyTest,
    },
)
