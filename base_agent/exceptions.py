

class TDFServerException(Exception):

    def __init__(self, msg, code):
        Exception.__init__(self, msg)
        self.msg = msg
        self.code = code

    def __repr__(self):
        return '[{}] - {}.'.format(self.code, self.msg)

    def __str__(self):
        return self.__repr__()
