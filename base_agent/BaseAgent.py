import os
import requests
import pandas as pd
import math

from . import TDFServerException

LOGIN_REPEAT_CODES = ['request_invalid_token', 'request_expired_token']


class BaseAgent:
    """The core functionality for agents trading on the TDF.

    Parameters
    ----------
    config : ConfigParser
        The configuration object for connection to an agent at TDF.
    url : str
        The TDF url.
    """

    def __init__(self, config, url):
        self.url = url

        self.password = config['TDFConnect']['password']
        self.email = config['TDFConnect']['email']
        self.agent_id = int(config['TDFConnect']['agent_id'])

        self.user = None
        self.token = None

        self._login()

    ###########################################################################
    #   Public Methods - Queries
    ###########################################################################

    def agent_info(self):
        """Fetches the current status of this agent and the league in which
        it belongs.

        Returns
        -------
        agent : dict
            The agent information. Contains the following key/values:
                'id': int
                    The id of the agent. Should be equal to `self.id`.
                'name': str
                    The name of the agent.
                'description': str
                    The agent description
                'value': number (2-decimal precision)
                    The total value of the portfolio, which is the cash plus
                    the value of all securities owned if they are sold
                    immediately (or bought if the securities are shorted).
                    This is the sum of 'cash' and all values in
                    'portfolioValues'.
                'cash': number (2-decimal precision)
                    The cash held by the agent.
                'portfolio': dict (str => int)
                    A mapping from ticker symbol to quantity owned for every
                    security owned by the agent.
                'portfolioValues': dict (str => number (2-decimal precision))
                    A mapping from ticker symbol to the value of the position
                    in that security. This value is the cash earned if all
                    of the securities of that symbol are sold immediately
                    (or bought if they are shorted, meaning they are held in
                    negative quantity).
                'leverage': number
                    The leverage position of the portfolio. This is computed
                    as x / n. x is cash if negative summed with all negative
                    values in portfolioValues, negated so that this is a
                    positive number. n is 'value'; i.e. the total portfolio
                    value. 'leverage' will always be non-negative. All leagues
                    limit leverage, and most limit it at zero (meaning that
                    cash cannot be negative/borrowed, and portfolio items
                    likewise cannot be negative/shorted).
                'user': dict
                    The id, email, and username of the owner of this agent.
                    Should be you.
        league : dict
            The league information. Contains the following key/valuesL:
                'id': int
                    The id of the league.
                'name': str
                    The name of the league.
                'description': str
                    The league description.
                'agents': list of int
                    A list of the id's of every agent registered in this
                    league. This list is sorted in order of highest value
                    to lowest value. Thus the agent that is currently winning
                    in the league is listed first.
                'maxAgentsPerUser': int
                    The maximum number of agents each user is allowed to
                    register.
                'startingCash': number (2-decimal precision)
                    The cash that each agent starts with at registration, after
                    each reset, and at the start of the competition period.
                'leverageMultiplier': number
                    The maximum allowed leverage (see the description of
                    'leverage' in the agent dict above).
                'hasPassphrase': boolean
                    True if the user is required to input a passphrase to
                    register an agent in teh league.
                'trialStart': number
                    The timestamp for the start of the league trial period.
                'trialStartStr': str
                    The start of the trial period
                'competitionStart': number
                    The timestamp for the start of the league competition
                    period.
                'competitionStartStr': str
                    The start of the competition period.
                'competitionEnd': number
                    The timestamp for the end of the league competition period.
                'competitionEndStr': number
                    The end of the competition period.
        position : int
            The position of the agent within the league. 1 means it has the
            highest value. This is the index of agent['id'] in
            league['agents'].

        Throws
        ------
        TDFServerException
            If the query fails in any way. The exception has a 'code' attribute
            which may take the following values:
                - 'agent_info_missing': If no agent with id as specified in
                  the configuration does not exist.
                - 'unknown': If anything else breaks. This shouldn't happen,
                  so contact the admin if you see it.
        """
        rs = self._post('agent/agent-info', id=self.agent_id)
        position = rs['league']['agents'].index(self.agent_id) + 1
        return rs['agent'], rs['league'], position

    def ticker_list(self):
        """Returns a list of all available tickers.

        Throws
        ------
        TDFServerException
            If the query fails in any way. The exception has a 'code' attribute
            which may take the following values:
                - 'unknown': If anything breaks. This shouldn't happen,
                  so contact the admin if you see it.
        """
        rs = self._post('price/ticker-list')
        return rs['tickers']

    def prices(self, ticker, type='today', quarter_hour=False,
               half_hour=False, hour=False, n=960):
        """Collects the historic bid, ask, and last prices for a single ticker.

        Contrast this function with `historic`, which collects only one of
        bid, ask, or last for many tickers.

        Parameters
        ----------
        ticker : str
            The ticker symbol of the security for which the price data is to
            be collected.
        type : str
            The type of price query to run. These are the same options that
            are available for the price plots at the TDF website on the
            "Historic Prices" page. Acceptable values and their meanings are as
            follows:
                - 'today': Collects all data from today (or last Friday if
                           today is a weekend).
                - 'yesterday': Collects all data from yesterday (or last Friday
                               if yesterday was a weekend).
                - 'this-week': Collects all data from this week. Weekends pull
                               from the week preceding.
                - 'this-month': Collects all data from this month, at a
                                resolution of one point every 15 minutes.
                - 'n': Collects the last n data points, where n is passed as a
                    separate parameter (see below).
        n : int, default=960
            The maximum number of records to fetch. Defaults to 960. Thresholds
            to be no greater than 3000 and no less than 1. Ignored unless
            type is 'n'.
        quarter-hour : boolean, default=False
            If True, only collects times every quarter hour on the quarter
            hour. Forced to be True on type='this-month'.
        half-hour : boolean, default=False
            If True, only collects times every half hour on the half hour.
            Forced to be False on type='this-month'.
        hour : boolean, default=False
            If True, only collects times every hour on the hour. Forced to be
            False on type='this-month'.

        Returns
        -------
        prices : pandas DataFrame
            The prices data where rows indices are datetimes (GMT), columns are
            bid, ask, and last, and the values are the respective prices.

            One "gotcha" is that the prices are sorted so that the most recent
            time is at the bottom.

            Another "gotcha" is that the times are expressed in GMT time, NOT
            Mountain Standard Time (unlike the front-end, which shows times
            in local time).

        Throws
        ------
        TDFServerException
            If the query fails in any way. The exception has a 'code' attribute
            which may take the following values:
                - 'historic_unknown_type': If `type` is not one recognized by
                  TDF.
                - 'historic_unknown_ticker': If `ticker` is not one collected
                  by TDF.
                - 'unknown': If anything else breaks. This shouldn't happen,
                  so contact the admin if you see it.
        """
        path = os.path.join('price/query/json', ticker)
        rs = self._post(path, type=type, n=n, quarter_hour=quarter_hour,
                        half_hour=half_hour, hour=hour)

        prices = pd.DataFrame(rs['prices'])
        prices['datetime'] = pd.to_datetime(prices['timestr'])
        prices = prices.set_index('datetime')

        return prices[['bid', 'ask', 'last']].sort_index()

    def historic(self, tickers=None, type='today', n=960, side='bid',
                 quarter_hour=False, half_hour=False, hour=False):
        """Collects the historic prices (one of bid, ask, last) for many
        tickers.

        Contrast this function with `prices`, which collects all of
        bid, ask, or last for a single ticker.

        Note: This function may be slow if you try to fetch too much data
        (too many times and/or too many tickers). You may want to consider
        cacheing old data and only collecting recent data in order to speed
        up operations (the longer you wait between data collection and
        trading, the more likely you are to loose money by acting on obsolete
        information).

        Parameters
        ----------
        tickers : list of str or None
            The list of ticker symbols for which price data will be collected.
            If None, data for all tickers will be collected (roughly 500, which
            come from the S&P 500).
        type : str
            The type of price query to run. These are the same options that
            are available for the price plots at the TDF website on the
            "Historic Prices" page. Acceptable values and their meanings are as
            follows:
                - 'today': Collects all data from today (or last Friday if
                           today is a weekend).
                - 'yesterday': Collects all data from yesterday (or last Friday
                               if yesterday was a weekend).
                - 'this-week': Collects all data from this week. Weekends pull
                               from the week preceding.
                - 'this-month': Collects all data from this month, at a
                                resolution of one point every 15 minutes.
                - 'n': Collects the last n data points, where n is passed as a
                    separate parameter (see below).
        n : int, default=960
            The maximum number of records to fetch. Defaults to 960. Thresholds
            to be no greater than 3000 and no less than 1. Ignored unless
            type is 'n'.
        quarter-hour : boolean, default=False
            If True, only collects times every quarter hour on the quarter
            hour. Forced to be True on type='this-month'.
        half-hour : boolean, default=False
            If True, only collects times every half hour on the half hour.
            Forced to be False on type='this-month'.
        hour : boolean, default=False
            If True, only collects times every hour on the hour. Forced to be
            False on type='this-month'.
        side : 'bid', 'ask', or 'last'
            The side of the price to collect.

        Returns
        -------
        prices : pandas DataFrame
            The prices data where rows indices are datetimes (GMT), columns are
            tickers, and the values are the respective prices (bid, ask, or
            last depending on what `side` is).

            One "gotcha" is that the prices are sorted so that the most recent
            time is at the bottom.

            Another "gotcha" is that the times are expressed in GMT time, NOT
            Mountain Standard Time (unlike the front-end, which shows times
            in local time).

        Throws
        ------
        TDFServerException
            If the query fails in any way. The exception has a 'code' attribute
            which may take the following values:
                - 'historic_unknown_type': If `type` is not one recognized by
                  TDF.
                - 'unknown': If anything else breaks. This shouldn't happen,
                  so contact the admin if you see it.
        """
        path = 'price/query'
        rs = self._post(path, type=type, n=n, tickers=tickers, side=side,
                        quarter_hour=quarter_hour, half_hour=half_hour,
                        hour=hour)

        # print(rs['quarter_hour'])
        # print(rs['data'])

        prices = pd.DataFrame(rs['prices']).transpose()
        prices = prices.fillna(method='ffill')

        if type == 'n':
            prices = prices.tail(n)

        return prices

    ###########################################################################
    #   Public Methods - Trading
    ###########################################################################

    def basic_trade(self, trade):
        """Executes a basic trade on TDF.

        Parameters
        ----------
        trade : dict (str => int)
            A map from ticker symbols to the quantity you wish to purchase
            (or sell if the quantity is negative). The trade will fail if
            leverage limits are exceeded (see the league rules on the TDF
            website for more information on leverage. However, it is likely
            that leverage is disabled, meaning you cannot spend more cash
            than you have or sell more securities than you own).

        Throws
        ------
        TDFServerException
            If the trade fails in any way. The exception has a 'code' attribute
            which may take the following values:
                - 'agent_trade_missing': If no agent with id as specified in
                  the configuration does not exist.
                - 'agent_trade_unauthorized': If you do not own the agent for
                  which you are attempting to trade (no hacking into other
                  people's agents!)
                - 'agent_trade_league_inactive': Either the league's trial
                  period has not begun or the league's competition period has
                  ended. See the TDF website for league dates.
                - 'agent_trade_short_borrow': If the league leverage limit is
                  0 (which it usually will be), but you attempted to spend more
                  cash than you have on hand or sell more securities than you
                  own.
                - 'agent_trade_leverage_exceeded': If the leverage limit is not
                  0 (you probably don't have to worry about this, but see the
                  league rules on TDF if you are curious), but your trade
                  exceeded the leverage limits.
                - 'unknown': If anything else breaks. This shouldn't happen,
                  so contact the admin if you see it.
        """
        path = 'agent/trade'
        self._post(path, trade=trade, id=self.agent_id)

    def state_trade(self, final_portfolio, agent_info=None):
        """Executes a trade so that the portfolio becomes as desired.

        Parameters
        ----------
        final_portfolio : dict (str => int)
            The desired portfolio after the trade is made. Keys are ticker
            symbols and values are the quantities to be owned (or shorted if
            negative) after the trade.
        agent_info : dict or None, default=None
            The `agent` object as returned by `agent_info()`. For
            performance reasons, pass this data as a parameter if fetched
            previously or needed later; otherwise, it is fetched and thrown
            away internally.

        Throws
        ------
        TDFServerException
            If the trade fails in any way. See `basic_trade()` for more info.
        """
        if agent_info is None:
            agent_info, league, place = self.agent_info()

        all_tickers = list(agent_info['portfolio'].keys()) + \
            list(final_portfolio.keys())

        trade = {
            ticker: (final_portfolio.get(ticker, 0) -
                     agent_info['portfolio'].get(ticker, 0))
            for ticker in all_tickers
        }
        self.basic_trade(trade)

    def composition_trade(self, final_composition, agent_info=None,
                          historic=None, side='bid'):
        """Executes a trade so that the final composition of security values
        is as specified in `final_composition`.

        Parameters
        ----------
        final_composition : dict (str => float)
            The desired final composition, which is dependent on the current
            values of the securities. The sum of values must be between 0 and
            1, inclusive. Each value represents the percentage of the final
            portfolio to be invested in that respective security. Any unused
            composition (1 - sum(values)) is placed into cash. To ensure that
            the trade is safely made, it would be a good idea to make sure
            that sum(values) < 1 (.95 seems to be a good number).
        agent_info : dict or None, default=None
            The `agent` object as returned by `agent_info()`. For
            performance reasons, pass this data as a parameter if fetched
            previously or needed later; otherwise, it is fetched and thrown
            away internally.
        historic : pandas DataFrame of length >= 1 or None
            Historic prices as returned by `self.historic()`. If not given,
            will fetch and throw away internally (so pass as a parameter
            if used elsewhere for performance reasons). Must have every
            security in `final_composition` as a column. Uses only the last
            row.
        side : 'bid', 'ask', or 'last'
            The price used to compute the value of securities. Only used if
            `historic` is None, then fetches prices at that side.

            NOTE: it is possible to justify any of the sides as prices. For
            example, if you use 'bid', then the value of the prices is the
            value you would get if you sold all of them immediately (even
            if you haven't bought them yet). As such, the final portfolio will
            have the exact composition as specified, excepting rounding error
            and ignoring price changes (since internally, TDF computes
            portfolio using the bid price for all securities owned, though
            it does use the ask price for securities shorted, but don't worry
            about this).

            If you use 'ask', then the value computation takes into account
            the purchase price and builds the portfolio as if the entire thing
            had to be built from scratch (even if you owned some of it right
            now). As the bid price is less than the ask, the final portfolio
            values will be less than computed here. This is a safer option,
            especially at the end of the day or for securities with a wide
            gap between bid and ask (otherwise, you may try to buy more
            than you can afford).

        Throws
        ------
        TDFServerException
            If the trade fails in any way. See `basic_trade()` for more info.
        """
        tickers = list(final_composition.keys())

        assert 0 <= sum(final_composition.values()) <= 1

        if historic is None:
            historic = self.historic(tickers=tickers, type='n', n=1, side=side)
        else:
            for ticker in tickers:
                assert ticker in historic.columns

        if agent_info is None:
            agent_info, league, place = self.agent_info()

        prices = historic.iloc[-1].to_dict()

        total_value = agent_info['value']
        state = {ticker: math.floor(
                            (final_composition[ticker] * total_value) /
                            prices[ticker]
                        )
                 for ticker in tickers}

        self.state_trade(state, agent_info)

    ###########################################################################
    #   Public Methods - Utilities
    ###########################################################################

    def returns(self, df):
        """Converts a DataFrame of prices (such as that returned by `prices` or
        `historic`) into a DataFrame of returns, where the return r[k] for each
        time k is (p[k] - p[k-1]) / p[k-1].

        Note: In order to function properly, the df must first be sorted so
        that most recent times are at the top. By default, `prices` and
        `historic` both returns dfs in this format.

        Note: The point at which no returns are gained or lost is at 0.
        Sometimes, the formulation of problems uses 1 instead, computing
        r1[k] = p[k] / p[k-1]. To create a df of r1 returns, simply use
        `df1 = df + 1`.

        Note: The returns dataframe will have one less row than the original
        dataframe as each return depends on a time previous and the oldest
        time has no previous time; therefore no returns can be computed for
        that row. One consequense of this is that df must have at least two
        rows.

        Parameters
        ----------
        df : pandas DataFrame
            The prices (either from `prices` or historic) dataframe with
            dates sorted from recent to oldest, and which contains at least
            two rows.

        Returns
        -------
        returns : pandas DataFrame
            The returns dataframe computed from prices.
        """
        return BaseAgent.compute_returns(df)

    @staticmethod
    def compute_returns(df):
        """The static version of returns.
        """
        dfs = df.shift(-1)
        return ((df / dfs) - 1).dropna()

    ###########################################################################
    #   Private Helpers
    ###########################################################################

    def _post(self, rel_url, _repeat=False, **kwargs):
        """
        Makes an authenticated post to the TDF.

        Parameters
        ----------
        kwargs : key word arguments
            The arguments to send through to the post.

        Returns
        -------
        rel_url : str
            The path, relative to the base url, of the post.
        _repeat : boolean
            Internal only. If the post fails due to an expired login, will
            attempt to login and try again, but with _repeat=True. If it still
            fails, then it will raise the TDFServerException.
        rs : dict
            The python dictionary representing the json response from the post.

        Raises
        ------
        TDFServerException
            If the response did not include a success.
        """
        if self.token is None:
            self._login()

        kwargs['token'] = self.token

        path = os.path.join(self.url, rel_url)
        r = requests.post(path, json=kwargs)
        rs = r.json()

        if 'success' in rs and rs['success']:
            return rs

        if not _repeat and rs['code'] in LOGIN_REPEAT_CODES:
            # Post failed due to bad token, try getting a new one and repeat
            self._login()
            return self._post(rel_url, _repeat=True, **kwargs)
        else:
            raise TDFServerException(rs['msg'], rs['code'])

    def _login(self):
        """
        Attempts to log the user in to TDF.
        """
        path = os.path.join(self.url, 'auth/login')
        r = requests.post(path,
                          json=dict(email=self.email, password=self.password))
        rs = r.json()

        if 'success' in rs and rs['success']:
            self.user = rs['user']
            self.token = rs['token']
        else:
            raise TDFServerException(rs['msg'], rs['code'])
