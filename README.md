## Description

The core functions and utilities for python TDF agents. Not meant to be a 
standalone project.

## Installation

Within the virtual environment for your agent 
(see <https://gitlab.com/idealabs/pytdf-sample-agent> for 
configuration instructions):

        $ python setup.py develop